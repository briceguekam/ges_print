<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("user/connect", "UserController@connect");
Route::get("user/store", "UserController@store");
Route::get("user/update", "UserController@update");

Route::get("facture/getAll", "FactureController@index");
Route::get("facture/printF/{id}", "FactureController@printF");
Route::get("facture/store", "FactureController@store");
