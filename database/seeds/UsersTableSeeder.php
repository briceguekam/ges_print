<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Insert simple user
        DB::table('users')->insert([
            'id' => null,
            'name' => "Simple Account",
            'phone' => "675789685",
            'mdp' => Hash::make('admin'),
            'genre' => 0,
            'is_admin' => 0,
        ]);
        //Insert simple user
        DB::table('users')->insert([
            'id' => null,
            'name' => "Simple Account",
            'phone' => "675789645",
            'mdp' => Hash::make('admin'),
            'genre' => 0,
            'is_admin' => 0,
        ]);
        //Insert Admin user
        DB::table('users')->insert([
            'id' => null,
            'name' => "Admin Account",
            'phone' => "675789658",
            'mdp' => Hash::make('admin'),
            'genre' => 0,
            'is_admin' => 1,
        ]);
    }
}
