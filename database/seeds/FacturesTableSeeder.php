<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FacturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fac1 = new \App\Facture([
            'id' => null,
            'designation' => "Copieur couleur ricoh Aficio MP C2800Occasion d’Europe ",
            'version' => "V154785A",
            'payment_version' => "0",
            'client_name' => "Kamga Greorge",
            'warranty' => "1500",
            'PU' => 200000,
            'Q' => 2,
            'color' => 150,
            'BW' => 195,
        ]);
        $fac1->save();

        $fac2 = new \App\Facture([
            'id' => null,
            'designation' => "Designation 1",
            'version' => "V154785A",
            'payment_version' => "2",
            'client_name' => "Kamga Greorge",
            'warranty' => "1500",
            'PU' => 200000,
            'Q' => 2,
            'color' => 150,
            'BW' => 195,
            'has_dem' => 1,
            'dem_name' => "Waffo Tamko",
            'dem_cni' => '102586958',
            "dem_amount" => "150000"
        ]);
        $fac2->save();
    }
}
