<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFactures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("designation");
            $table->string("version");
            $table->string("payment_version");
            $table->string("client_name");
            $table->string("warranty");
            $table->integer("PU");
            $table->integer("Q");
            $table->integer("color");
            $table->integer("BW");
            $table->boolean("has_dem")->default(0);
            $table->string("dem_name")->default("N/A");
            $table->string("dem_cni")->default("N/A");
            $table->string("dem_amount")->default("N/A");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_factures');
    }
}
