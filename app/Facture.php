<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    protected $table = "factures";
    protected $primaryKey = "id";
    protected $guarded = [];
}
