<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function connect(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'login' => 'required|max:100|string',
            'mdp' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->render($validator->errors()->first());
        }
        $user = User::where("phone", $request->login)->get();
        if (count($user) == 0) {
            return $this->render("Login incorrect");
        }
        if (!Hash::check($request->mdp, $user[0]->mdp)) {
            return $this->render("Mot de passe incorrect");
        }
        return $this->render($user[0], true);
    }

    public function store(Request $request)
    {
        $request = json_decode($request->data);

        $tUser = User::where("phone", $request->phone)->get();
        if (count($tUser) != 0) {
            return $this->render("Ce numéro de telephone est déja enregistre");
        }
        if ($request->mdp != $request->mdp_conf) {
            return $this->render("Les mot de passe ne correspondent pas.");
        }
        $user = new User([
            'name' => $request->name,
            'phone' => $request->phone,
            'mdp' => Hash::make($request->mdp),
            'genre' => $request->genre ,
            'is_admin' => 0,
        ]);
        $user->save();
        return $this->render("Enregistrement effectuer avec succès", true);
    }

    public function update(Request $request)
    {
        $request = json_decode($request->data);

        $user = User::find("id", $request->id);
        if (count($user) != 0) {
            return $this->render("Impossible de modifier ce compte");
        }
        if ($request->mdp != $request->mdp_conf) {
            return $this->render("Les mot de passe ne correspondent pas.");
        }
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->mdp = $request->mdp;
        $user->genre = $request->genre;

        $user->update();
        return $this->render("Mis a jour effectuer avec succès", true);
    }
}
