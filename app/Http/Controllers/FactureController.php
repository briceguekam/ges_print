<?php

namespace App\Http\Controllers;

use App\Facture;
use Illuminate\Http\Request;

class FactureController extends Controller
{
    public function index(Request $request){
        $fac = Facture::whereDate('created_at', '<=', $request->maxDate)
            ->whereDate('created_at', '>=', $request->minDate)
            ->orderBy("created_at")
            ->get();
        return $this->render($fac, true);
    }

    public function printF($id)
    {
        $fac = Facture::find($id);
        return view('facture', ["fac" => $fac]);
    }
    public function store(Request $request){
        $data = json_decode($request->data);

        $fac2 = new Facture([
            'designation' => $data->designation,
            'version' => $data->version,
            'payment_version' => $data->payment_version,
            'client_name' => $data->client_name,
            'warranty' => $data->warranty,
            'PU' => $data->PU,
            'Q' => $data->Q,
            'color' => $data->color,
            'BW' => $data->BW,
        ]);
        
        if($data->demCheck){
            $fac2->has_dem = 1;
            $fac2->dem_name = $data->dem_name;
            $fac2->dem_cni = $data->dem_cni;
            $fac2->dem_amount = $data->dem_amount;
        }

        if($fac2->save()){
            return $this->render("Facture enregistre", true);
        }else{
            return $this->render("Facture enregistre");
        }


    }

}
