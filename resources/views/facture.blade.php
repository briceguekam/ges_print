<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$fac->client_name}}</title>
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
<div id="background">

</div>
<header>
    <img width="100%" src="{{asset("img/header.jpg")}}" alt="">
</header>
<div class="mb-2" id="date">
    Yaounde le {{$fac->created_at->format('d-m-Y')}}
</div>
<h2 class="mb-5">Facture</h2>
<div class="mb-4">
    <h1>Doit : {{$fac->client_name}}</h1>
</div>
<div>
    <table style="margin: 0 auto; width: 100%" class="table table-bordered">
        <thead>
        <tr>
            <th scope="col" colspan="14">Désignation</th>
            <th scope="col" colspan="1">Qté</th>
            <th scope="col" colspan="2">PU(FCFA)</th>
            <th scope="col" colspan="2">PT(FCFA)</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="blue" colspan="14">{{$fac->designation}}</td>
            <td class="blue" rowspan="4" colspan="1">{{$fac->Q}}</td>
            <td class="blue" rowspan="4" colspan="2">{{$fac->PU}}</td>
            <td class="blue" rowspan="4" colspan="2">{{$fac->PU * $fac->Q}}</td>
        </tr>
        <tr>
            <td colspan="8">Compteur</td>
        </tr>
        <tr>
            <td class="blue">B&W:</td>
            <td class="blue">{{$fac->BW}}</td>
            <td class="blue">COLOR:</td>
            <td class="blue">{{$fac->color}}</td>
        </tr>
        <tr>
            <td>N°SERIE</td>
            <td colspan="3">{{$fac->version}}</td>
        </tr>
        <tr>
            <td colspan="13">TOTAL</td>
            <td id="total" colspan="5">{{$fac->PU * $fac->Q}}</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="mb-5 mt-5">
    <h2>Arrêter la présente facture au montant total de : <br> {{$fac->PU * $fac->Q}} FCFA</h2>
</div>
<div id="reglement" class="mt-3">
    <span>Mode de règlement :</span>&nbsp;&nbsp;
    <span style="text-decoration: {{$fac->payment_version == 0 ? "underline" : "line-through"}}">Espèces</span>
    <input type="radio" {{$fac->payment_version == 0 ? "checked" : "" }} >&nbsp;&nbsp;

    <span style="text-decoration: {{$fac->payment_version == 1 ? "underline" : "line-through"}}">Chèque</span>
    <input type="radio" {{$fac->payment_version == 1 ? "checked" : "" }}>&nbsp;&nbsp;

    <span style="text-decoration: {{$fac->payment_version == 2 ? "underline" : "line-through"}}">mobile money</span>
    <input type="radio" {{$fac->payment_version == 2 ? "checked" : "" }}>
</div>
<div class="mt-5">
    <h3 style="text-align: center">Garanties : {{$fac->warranty}} copies</h3>
</div>
<div class="mt-5 mb-5" id="signature">
    <div class="signature">
        Client
    </div>
    <div class="signature">
        La Direction
    </div>
</div>
<footer>
    <img width="100%" src="{{asset("img/footer.jpg")}}" alt="">
</footer>
</body>
</html>